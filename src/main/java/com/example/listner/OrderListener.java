package com.example.listner;


import com.example.model.Order;
import com.example.model.User;
import com.example.repo.OrderRepo;
import com.example.repo.UserRepo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class OrderListener {

  @Value("${order.topic.name}")
  private String topicName;

  @Autowired
  OrderRepo orderRepo;
  @Autowired
  UserRepo userRepo;

  @KafkaListener(topics = "order-topic", groupId = "first")
  public void listenGroupFoo(String message) {
    System.out.println("Received Message in group : " + message);
    ObjectMapper object = new ObjectMapper();
    Order order = null;
    try {
      order = object.readValue(message, Order.class);
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }
    User user = userRepo.findById(order.getUserId()).get();
    if (user.getBalance() > order.getOrderAmount()) {
      user.setBalance(user.getBalance() - order.getOrderAmount());
      order.setStatus("SUCCESS");
      userRepo.save(user);
      orderRepo.save(order);
    } else {
      order.setStatus("FAILED");
      orderRepo.save(order);
    }

  }
}
