package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaDatabaseConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(KafkaDatabaseConsumerApplication.class, args);
	}

}
